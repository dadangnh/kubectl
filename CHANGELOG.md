# Kubectl Changelog

## Version 1.0.1 (Current Release)
  * Based on Bitnami kubectl:1.25.4 image

## Version 1.0.0 (Initial Release)
  * Based on Bitnami kubectl:latest image
