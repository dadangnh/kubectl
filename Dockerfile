FROM bitnami/kubectl:1.25.4
MAINTAINER Dadang NH <hello@dadangnh.com>

# User root is required for installing packages
USER root
RUN install_packages curl

# Return to previous non-root user
USER 1001
